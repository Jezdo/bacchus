<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
//	$r = new ReflectionClass('Redis');
//	var_dump($r);

	echo "Bacchus Server is running...";
});

Route::resource('zones', 'ZonesController');
Route::resource('temperatures', 'TemperaturesController');
//Route::get('temperatures/current', 'TemperaturesController@current');
Route::get('temperature', 'TemperaturesController@current');

Route::get('led/{id}', 'LedController@show');
Route::get('led/{id}/enable', 'LedController@enable');
Route::get('led/{id}/disable', 'LedController@disable');