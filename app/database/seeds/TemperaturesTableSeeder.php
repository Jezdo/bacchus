<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class TemperaturesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			$temperature = new Temperature();
			$temperature->zone_id = 1;
			$temperature->temperature = rand(23000,30000)/1000;
			$temperature->save();
		}
	}

}