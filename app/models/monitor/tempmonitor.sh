#!/bin/sh

if [ ! -d "/sys/bus/w1/devices/$1" ]; then
    echo "UNREADABLE"
fi
cat /sys/bus/w1/devices/$1/w1_slave