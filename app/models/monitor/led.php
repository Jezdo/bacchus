<?php

/**
 * Class LedRegister
 * Contains list of all LEDs and their ports.
 */
class LedRegister {
    const LED_GREEN = 0;
}

class Led {

    private $ledPort;
    private $ledState = 0;

    public function __construct($port){
        $this->ledPort = $port;
        exec("gpio mode ".$this->ledPort." out");
    }

    /**
     * Flashes LED for x amount of seconds
     * @param int $timeout
     */
    public function flash($timeout = 3){
        $this->ledState = 1;
        $this->send();
        sleep($timeout);
        $this->ledState = 0;
        $this->send();
    }

    public function toggle(){
        $this->ledState = ($this->ledState == 0) ? 1 : 0;
        $this->send();
    }

    /**
     * Alises for state change
     */
    public function enable(){
        $this->ledState = 1;
        $this->send();
    }
    public function disable(){
        $this->ledState = 0;
        $this->send();
    }

    /**
     * This method is used to send signal to LED
     */
    private function send(){
        exec("gpio write ".$this->ledPort." ".$this->ledState);
    }

}