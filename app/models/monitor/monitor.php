<?php
include 'led.php';
/**
 * Class Monitor
 * We run this only via cron job (or CLI)
 * This is used to monitor room temperature and write it in DB
 */
class Monitor {

    private $sensorId = '28-0000050b6102';


    public function __construct(){

        $led = new Led(LedRegister::LED_GREEN);
        $led->enable();

        $temperature = $this->getCurrentTemperature();
        $zone = Zone::where('senzor_id', '=', $this->sensorId)->first();
        if(!is_object($zone)){
            echo "Unable to locate sensor's zone.";
            die;
        }

        echo $this->saveTemperature($zone, $temperature);

//        $led->disable();

    }

    public function saveTemperature(Zone $zone, $temperature){
        echo "Creating new temperature...";
        $newTemperature = new Temperature();
        $newTemperature->zone_id = $zone->id;
        $newTemperature->temperature = $temperature/1000;
//        var_dump($newTemperature);
        $newTemperature->save();
        echo "Saved..";
//        var_dump($newTemperature);

        /**
         * Output for log files, if any...
         */
        $output = "Date: ".date("c")."\n";
        $output .= "Zone: ".$zone->name." (#".$zone->id.")\n";
        $output .= "Temperature: ".($temperature/1000)."\n\n";
        return $output;
    }

    public function getCurrentTemperature(){
        /**
         * Implement ability to pass as param SN
         */
        exec("cd ".__DIR__."; ./tempmonitor.sh ".$this->sensorId, $a);
        if(strpos($a[0], "UNREADABLE") !== false){
            throw new \Exception("Unable to read sensor ".$this->sensorId.": Directory does not exists");
        }
        $info = $a[1];
        if(strpos($info, "=") === false){
            throw new \Exception("Unable to read sensor ".$this->sensorId.": Invalid file format.");
        }
        $temp = explode("=", $info);
        return $temp[1];
    }
}

