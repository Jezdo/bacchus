<?php

class TemperaturesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /temperatures
	 *
	 * @return Response
	 */
	public function index()
	{
		return Temperature::all();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /temperatures/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /temperatures
	 *
	 * @return Response
	 */
	public function store()
	{
		$zoneId = Input::get('zone_id');
		$temperature = Input::get('temperature');
		$newTemperature = new Temperature();
		$newTemperature->zone_id = $zoneId;
		$newTemperature->temperature = $temperature/1000;
		$newTemperature->save();
		return $newTemperature;
	}

	/**
	 * Display the specified resource.
	 * GET /temperatures/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Temperature::find($id);
	}

	public function current(){

		$temperatureInfo = DB::table('temperatures')->orderBy('id', 'desc')->limit(1)->get();

		return $temperatureInfo;
	}
	/**
	 * Show the form for editing the specified resource.
	 * GET /temperatures/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /temperatures/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /temperatures/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}