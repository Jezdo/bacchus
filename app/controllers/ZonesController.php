<?php

class ZonesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /zones
	 *
	 * @return Response
	 */
	public function index()
	{
		return Zone::all();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /zones/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	public function validate($name, $senzorId){

		if(empty($name)){
			throw new Exception('You have to specify name.', ErrorCodes::BAD_REQUEST);
		}

		// Dont allow same zone names
		if(Zone::where('name', '=', $name)->first()){
			throw new Exception('Zone named '.$name.' already exists.', ErrorCodes::ALREADY_EXISTS);
		}
		return true;

	}
	/**
	 * Store a newly created resource in storage.
	 * POST /zones
	 *
	 * @return Response
	 */
	public function store()
	{
		$name = Input::get('name');
		$senzorId = Input::get('senzorId');

		$this->validate($name, $senzorId);

		$newZone = new Zone();
		$newZone->name = $name;
		$newZone->senzor_id = $senzorId;
		$newZone->save();
		return $this->ok($newZone);
	}

	/**
	 * Display the specified resource.
	 * GET /zones/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Zone::find($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /zones/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /zones/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$name = Input::get('name');
		$senzorId = Input::get('senzorId');

		$this->validate($name, $senzorId);

		$newZone = Zone::findOrFail($id);
		$newZone->name = $name;
		$newZone->senzor_id = $senzorId;
		$newZone->save();
		return $this->ok($newZone);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /zones/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Zone::findOrFail($id)->delete();
		return $this->ok("Deleted.");
	}

}