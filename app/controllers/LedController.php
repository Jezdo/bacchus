<?php

class LedController extends \BaseController {

	private $ledManager;
	public function __construct(){
	}

	/**
	 * Display a listing of the resource.
	 * GET /led
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /led/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		# TODO: We can switch $id to get correct LED
		$this->ledManager = new Led(LedRegister::LED_GREEN);
		return $this->ok($this->ledManager->getLedState());
	}

	/**
	 * @param $id
	 */
	public function enable($id){
		# TODO: We can switch $id to get correct LED
		$this->ledManager = new Led(LedRegister::LED_GREEN);
		$this->ledManager->enable();
		return $this->ok("Enabled");
	}

	/**
	 * @param $id
	 */
	public function disable($id){

		# TODO: We can switch $id to get correct LED
		$this->ledManager = new Led(LedRegister::LED_GREEN);
		$this->ledManager->disable();
		return $this->ok("Disabled");
	}

}