<?php


class BaseController extends Controller {
//	protected $layout = '';

	protected function ok($message, $code = 200){
		return Response::json(['status' => $code, 'message' => $message], $code);
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
//	protected function setupLayout()
//	{
//		if ( ! is_null($this->layout))
//		{
//			$this->layout = View::make($this->layout);
//		}
//	}

}
